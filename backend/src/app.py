import connexion
from flask_cors import CORS

app = connexion.App(
    __name__,
    specification_dir='.',
    strict_validation=True
)
fapp = app.app  # flask app
CORS(fapp)

