from encoder import Encoder
from app import app


if __name__ == '__main__':
    app.app.json_encoder = Encoder

    api = app.add_api('apispec.yaml')
    app.run(port=8000, host='0.0.0.0')
