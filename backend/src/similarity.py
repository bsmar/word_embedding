
from gensim.models import Word2Vec


class Similarity:
    """
    Provides finding sentence similarity in sentences array
    """

    def find_similarity(self, sentences, search):
        sentences_splitted = [s.split(' ') for s in sentences]
        search_splitted = search.split(' ')
        model = Word2Vec(sentences_splitted, min_count=1)

        similarities = []
        for s in sentences_splitted:

            similarities.append(dict(
                sim=model.wv.n_similarity(search_splitted, s),
                sentence=' '.join(s)
            ))

        return sorted(similarities, key=lambda x: x['sim'], reverse=True)[1:]
