from unittest import TestCase
from similarity import Similarity


class TestSimilarity(TestCase):

    def test_similarity(self):
        """
        Test for find similarity in array of sentences by sentence
        """
        sentences = ['computer artificial intelligence system',
                     'artificial trees',
                     'human intelligence',
                     'artificial graph',
                     'intelligence',
                     'artificial intelligence system']

        search = 'artificial intelligence system'

        expected_result = 'computer artificial intelligence system'
        result = Similarity().find_similarity(sentences, search)

        self.assertEqual(expected_result, [r['sentence'] for r in result][0])
