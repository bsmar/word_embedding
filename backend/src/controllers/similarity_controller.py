import connexion

from similarity import Similarity


def get_similarities():
    """
    This operation allows you to list of sentences sorted by similarity
    :rtype: Dict
    """

    form_data = connexion.request.get_json()

    result = Similarity().find_similarity(
        form_data['sentences'],
        form_data['search']
    )

    return result
