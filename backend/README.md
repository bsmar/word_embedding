This service provides similarity search in the text by sentence

Install & run

Installing dependencies
>pip install -r requirements.txt;

Run server
>cd src

>python runserver.py

API
For look API run server and open your browser to here: 
>http://localhost:8080/ui/

Your Swagger definition lives here: 
>http://localhost:8080/swagger.json
