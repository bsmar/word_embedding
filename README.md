## The word embedding app 

The app consist of two services:
* frontend - react, redux
* backend - flask, swagger 

### Install & run backend

Installing dependencies
```
cd backend
pip install -r requirements.txt;
```

Run the server
```
cd src
python runserver.py
```

API

For look API run server and open your browser to here: 
>http://localhost:8080/ui/

### Install & run frontend
Installing dependencies
```
cd frontend
npm install
```

Run the server
```
npm start
```

Open in browser next url:
>http://localhost:8080
