import React, {Component} from 'react';
import {connect} from 'react-redux';


class SentenceSimilarity extends Component {

  renderList() {
    return this.props.similarities.map(similarity => {
      return(
        <li key={similarity.sentence}>
          {similarity.sentence}
        </li>
      )
    })
  }

  render() {
    if (this.props.similarities.length === 0) {
      return ''
    }

    return (
      <div>
        <h4><small>Similarities:</small></h4>
        <ul>
          {this.renderList()}
        </ul>
      </div>
    )
  }
}


function mapStateToProps({similarities}) {
  return {similarities}
}

export default connect(mapStateToProps)(SentenceSimilarity);
