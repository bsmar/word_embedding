import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {changeTextToProcess, clearSimilarity} from '../actions/index'


class TextToProcess extends Component {

  constructor(props) {
    super(props);

    this.state = {term: ''};

    this.onTextAreaPaste = this.onTextAreaPaste.bind(this);
    this.clearTextArea = this.clearTextArea.bind(this);
  }

  onTextAreaPaste(e) {
    let term = e.clipboardData.getData('Text');
    this.setState({term: term});
    this.props.changeTextToProcess(term);
    this.props.clearSimilarity();
  }

  clearTextArea() {
    this.setState({term: ''});
    this.props.changeTextToProcess('');
    this.props.clearSimilarity();
  }


  render() {
    return (
      <form className='form-group'>
        <div className="form-group">
          <label htmlFor="text-to-process">Paste text to process:</label>
          <div>
            <a href="#" onClick={this.clearTextArea}>Clear</a>
          </div>
          <textarea
            id='text-to-process'
            className='form-control'
            onPaste={this.onTextAreaPaste}
            value={this.state.term}
            cols="60"
            rows="5">

          </textarea>
        </div>
      </form>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({changeTextToProcess, clearSimilarity}, dispatch)
}

export default connect(null, mapDispatchToProps)(TextToProcess)
