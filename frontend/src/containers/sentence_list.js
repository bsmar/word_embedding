import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {fetchSimilarity} from '../actions/index'



class SentenceList extends Component {

  renderList() {
    const sentences = this.props.sentences;
    return sentences.map(sentence => {
      return (
        <li
          key={sentence}
          onClick={() => this.props.fetchSimilarity(sentence, sentences)}>
          <a href="#">{sentence}</a>
        </li>
      )
    })
  }

  render()  {
    if (this.props.sentences.length === 0) {
      return ''
    }

    return (
      <div>
        <h4>
          <small>Click on the sentence to see similarity</small>
        </h4>
        <ul>
          {this.renderList()}
        </ul>
      </div>
    )
  }
}

function mapStateToProps({sentences}) {
  return {sentences}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({fetchSimilarity: fetchSimilarity}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SentenceList);
