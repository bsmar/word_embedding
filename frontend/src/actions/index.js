import axios from 'axios';

export const CHANGE_TEXT_TO_PROCESS = 'CHANGE_TEXT_TO_PROCESS';
export const FETCH_SIMILARITY = 'FETCH_SIMILARITY';
export const CLEAR_SIMILARITY = 'CLEAR_SIMILARITY';

const ROOT_URL = 'http://0.0.0.0:8000';


export function changeTextToProcess(text) {
  let result = text.match( /[^\.!\?]+[\.!\?]+/g );
  if (!result) {
    result = []
  }
  return {
    type: CHANGE_TEXT_TO_PROCESS,
    payload: result
  }
}


export function fetchSimilarity(search, sentences) {

  const url = `${ROOT_URL}/sentence-similarities`;

  const request = axios.post(url, {search: search, sentences: sentences});

  return {
    type: FETCH_SIMILARITY,
    payload: request
  }
}

export function clearSimilarity() {
  return {
    type: CLEAR_SIMILARITY,
    payload: []
  }
}