import { combineReducers } from 'redux';
import sentencesReducer from './reducer_text_to_process'
import similarityReducer from './reducer_similarity'


const rootReducer = combineReducers({
  sentences: sentencesReducer,
  similarities: similarityReducer
});

export default rootReducer;
