import {FETCH_SIMILARITY} from "../actions";
import {CLEAR_SIMILARITY} from "../actions";


export default function (state = [], action) {

  switch (action.type) {
    case FETCH_SIMILARITY:
      if (!action.payload.data) {
        return [];
      }
      return action.payload.data;
    case CLEAR_SIMILARITY:
      return action.payload;
  }

  return state
}
