import {CHANGE_TEXT_TO_PROCESS} from "../actions";


export default function (state = [], action) {

  switch (action.type) {
    case CHANGE_TEXT_TO_PROCESS:
      return action.payload;
  }

  return state
}