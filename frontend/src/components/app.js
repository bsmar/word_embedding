import React, { Component } from 'react';
import TextToProcess from '../containers/text_to_process';
import SentenceList from '../containers/sentence_list'
import SentenceSimilarity from '../containers/sentence_similarity'


export default class App extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <TextToProcess />
        </div>
        <div className="row">
          <div className="col-md-6">
            <SentenceList />
          </div>
          <div className="col-md-6">
            <SentenceSimilarity />
          </div>
        </div>
      </div>
    );
  }
}
